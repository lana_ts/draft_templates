#-*- coding: utf-8 -*-

from django.shortcuts import render
from .forms import partnersForm, UpdPartnersForm
from .models import partners
from django.core.urlresolvers import reverse
from django.shortcuts import HttpResponseRedirect, RequestContext
from django.shortcuts import render_to_response, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.cache import cache_page
from django.template import Context ,RequestContext


   
def partners1(request):
    partners_list = partners.objects.all()

    
    if request.POST:
        form = partnersForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/partners/')
        else:
            context = {'form': form}
            return render(request, 'partners.html', context)
    else:
        form = partnersForm()
        context = {'form': form, 'partners':partners_list}
        return render(request, 'partners/partners.html', context)
# Create your views 

def del_partners(request, p_id):
    p = partners.objects.get (id = p_id)
    p.delete()
    return HttpResponseRedirect(reverse('partners:partners'))

    
def partners_detail(request, p_id):
    try:
        spikers = partners.objects.filter(id = p_id)
    except partners.objects.filter(id = p_id).DoesNotExist:
        raise Http404("partners does not exist")
    n = partners.objects.get(id = p_id)
    if request.method == 'POST':
        postdata = request.POST.copy()
        form = UpdPartnersForm(request.POST, instance = n) 
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('partners:partners'))
        else:
            return render_to_response('partners/partners.html',{'form':form, 'partners':partners.objects.filter(id=spiker_id)},context_instance = RequestContext(request))
    else:    
        form = UpdPartnersForm(instance=n)
        return render_to_response('partners/partners.html',{'form':form, 'partners':partners.objects.filter(id=p_id)},context_instance = RequestContext(request))
