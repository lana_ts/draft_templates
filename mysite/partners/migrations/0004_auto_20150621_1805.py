# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('partners', '0003_auto_20150602_0938'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='partners',
            options={'verbose_name_plural': 'Партнеры'},
        ),
    ]
