# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('partners', '0002_auto_20150525_1319'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='partners',
            options={'verbose_name': 'Партнер', 'verbose_name_plural': 'Партнеры'},
        ),
        migrations.RenameField(
            model_name='partners',
            old_name='image',
            new_name='imgage',
        ),
    ]
