from django.contrib import admin
from partners.models import partners

class partnersAdmin(admin.ModelAdmin):
    search_fields = ['description']

admin.site.register(partners, partnersAdmin)

# Register your models here.


