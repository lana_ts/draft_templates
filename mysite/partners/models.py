from django.db import models
from django import forms

class partners(models.Model):
    imgage = models.ImageField(upload_to='partners_img',null=True, blank=True)
    title = models.CharField(max_length=70)
    description = models.CharField(max_length=255)
    
    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name_plural = 'Партнеры'
    
class AddPartner(forms.Form):
    imgage = models.ImageField(upload_to='partners_img',null=True, blank=True)
    title = models.CharField(max_length=70)
    description = models.CharField(max_length=255)