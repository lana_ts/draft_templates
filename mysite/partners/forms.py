from django import forms
from django.forms import ModelForm
from partners.models import partners

class NameForm(forms.Form):
    imgage = forms.ImageField(label='imgage')
    title = forms.CharField(label='title', max_length=100)
    description = forms.CharField(label='description', max_length=100)


class partnersForm(ModelForm):
    class Meta:
        model = partners
        fields = ['imgage', 'title', 'description']
        
class UpdPartnersForm(ModelForm):
    class Meta:
        model = partners
        fields = ['imgage', 'title', 'description']