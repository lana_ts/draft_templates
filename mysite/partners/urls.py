from django.conf.urls import patterns, url
from partners import views
# Create your views here.


urlpatterns = [
    url(r'^$', views.partners1, name='partners'),
    url(r'(?P<p_id>\d+)/del/$', views.del_partners, name = 'del_partners'),
    url(r'^(?P<p_id>\d+)/$', views.partners_detail, name = 'partners_detail'),
]
# Create your urls here.
