from django import forms
from django.forms import ModelForm
from kontakts.models import kontakts

class NameForm(forms.Form):
    name = forms.CharField(max_length=200)
    description = forms.Textarea()


class kontaktsForm(ModelForm):
    class Meta:
        model = kontakts
        fields = ['name','description']
        
