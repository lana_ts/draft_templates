from django.db import models

class kontakts(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    
    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name = 'Контакт'
        verbose_name_plural = 'Контакты'