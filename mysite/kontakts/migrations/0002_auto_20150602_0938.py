# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kontakts', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Contacts',
            new_name='kontakts',
        ),
        migrations.AlterModelOptions(
            name='kontakts',
            options={'verbose_name': 'Контакт', 'verbose_name_plural': 'Контакты'},
        ),
    ]
