#-*- coding: utf-8 -*-

from django.shortcuts import render
from .models import kontakts
from .forms import kontaktsForm

def add_kontakts(request):
    kontakts_list = kontakts.objects.all()
    if request.POST:
        form = kontaktsForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/kontakts/')
        else:
            context = {'form': form}
            return render(request, 'kontakts.html', context)
    else:
        form = kontaktsForm()
        context = {'form': form, 'kontakts':kontakts_list}
        return render(request, 'kontakts/kontakts.html', context)

