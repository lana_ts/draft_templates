#-*- coding: utf-8 -*-

from django.shortcuts import render
from .forms import organisatorsForm
from .models import organisators

def organisators(request):
    part = organisators.objects.all()
    if request.POST:
        form = organisatorsForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/organisators/')
        else:
            context = {'form': form, 'organisators':part}
            return render(request, 'organisators/organisators.html', context)
    else:
        form = organisatorsForm()
        context = {'form': form, 'organisators':part}
        return render(request, 'organisators/organisators.html', context)