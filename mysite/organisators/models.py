from django.db import models

class organisators(models.Model):
    title = models.CharField(max_length=70)
    description = models.CharField(max_length=255)
    
    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name = 'Организатор'
        verbose_name_plural = 'Организаторы'
 
# Create your models here.
