from django import forms
from django.forms import ModelForm
from organisators.models import organisators


class NameForm(forms.Form):
    title = forms.CharField(max_length=70)
    description = forms.CharField(max_length=255)


class organisatorsForm(ModelForm):
    class Meta:
        model = organisators
        fields = ['title', 'description']