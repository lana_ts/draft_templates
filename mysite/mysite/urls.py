from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.views.generic import TemplateView
from django.contrib.flatpages import views



urlpatterns = [
 url(r'^admin/', include(admin.site.urls)),
url(r'^partners/', include('partners.urls', namespace="partners")),
url(r'^spikers/', include('spikers.urls', namespace="spikers")),
url(r'^vistuplenia/', include('vistuplenia.urls', namespace="vistuplenia")),
url(r'^organisators/', include('organisators.urls', namespace="organisators")),
url(r'^news/', include('news.urls', namespace="news")),
url(r'^kontakts/', include('kontakts.urls', namespace="kontakts")),

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()

