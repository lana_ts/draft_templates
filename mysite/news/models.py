from django.db import models
class news(models.Model):
    title = models.CharField(max_length=70)
    description = models.CharField(max_length=255)
    
    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'

# Create your models here.
