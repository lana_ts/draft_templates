#-*- coding: utf-8 -*-

from django.shortcuts import render
from .forms import newsForm
from .models import news

from django.core.urlresolvers import reverse
from django.shortcuts import HttpResponseRedirect, RequestContext
from django.shortcuts import render_to_response, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.cache import cache_page
from django.template import Context ,RequestContext


    
    

    
def add_news(request):        
    news_list = news.objects.all()
    paginator = Paginator(news_list, 1)
    page = request.GET.get('page')
    try:
        news_pag = paginator.page(page)
    except PageNotAnInteger:
        news_pag = paginator.page(1)
    return render_to_response('news/news.html', {'add_news': news_pag})
