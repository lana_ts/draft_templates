from django import forms
from django.forms import ModelForm
from news.models import news


class NameForm(forms.Form):
    title = forms.CharField(max_length=70)
    description = forms.CharField(max_length=255)



class newsForm(ModelForm):
    class Meta:
        model = news
        fields = ['title', 'description']