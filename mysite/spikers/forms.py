from django import forms
from django.forms import ModelForm
from spikers.models import spikers


class NameForm(forms.Form):
    title = forms.CharField(max_length=70)
    description = forms.CharField(max_length=255)
    pub_date = forms.DateTimeField()
    text = forms.Textarea()
     


class spikersForm(ModelForm):
    class Meta:
        model = spikers
        fields = ['title', 'description', 'pub_date', 'text']