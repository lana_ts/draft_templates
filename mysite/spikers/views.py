#-*- coding: utf-8 -*-

from django.shortcuts import render
from .forms import spikersForm
from .models import spikers

def spikers1(request):
    spikers_list = spikers.objects.all()
    if request.POST:
        form = spikersForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/spikers/')
        else:
            context = {'form': form, 'spikers':spikers_list}
            return render(request, 'spikers/base_spikers.html', context)
    else:
        form = spikersForm()
        context = {'form': form, 'spikers':spikers_list}
        return render(request, 'spikers/spikers.html', context)