from django.db import models

class spikers(models.Model):
    title = models.CharField(max_length=70)
    description = models.CharField(max_length=255)
    pub_date = models.DateTimeField()
    text = models.TextField()
 
    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name = 'Спикер'
        verbose_name_plural = 'Спикеры'

# Create your models here.
