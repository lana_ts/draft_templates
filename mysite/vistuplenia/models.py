from django.db import models

class vistuplenia(models.Model):
    title = models.CharField(max_length=70)
    description = models.CharField(max_length=255)
    date = models.DateTimeField()

    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name = 'Выступление'
        verbose_name_plural = 'Выступления'

# Create your models here.
