from django import forms
from django.forms import ModelForm
from vistuplenia.models import vistuplenia

class NameForm(forms.Form):
    title = forms.CharField(max_length=70)
    description = forms.CharField(max_length=255)
    date = forms.DateTimeField()


class vistupleniaForm(ModelForm):
    class Meta:
        model = vistuplenia
        fields = ['title', 'description', 'date']