# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vistuplenia', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='vistuplenia',
            options={'verbose_name': 'Выступление', 'verbose_name_plural': 'Выступления'},
        ),
    ]
