#-*- coding: utf-8 -*-

from django.shortcuts import render
from .forms import vistupleniaForm
from .models import vistuplenia

def add_spikers(request):
    part = vistuplenia.objects.all()
    if request.POST:
        form = vistupleniaForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/vistuplenia/')
        else:
            context = {'form': form, 'spikers':part}
            return render(request, 'vistuplenia/base_vistuplenia.html', context)
    else:
        form = vistupleniaForm()
        context = {'form': form, 'vistuplenia':part}
        return render(request, 'vistuplenia/base_vistuplenia.html', context)
